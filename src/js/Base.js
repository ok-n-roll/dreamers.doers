import Main from './sections/Main.js';
import Projects from './sections/Projects.js';
//import Approach from './sections/Approach.js';
import Skills from './sections/Skills.js';
import Team from './sections/Team.js';
import Contacts from './sections/Contacts.js';

export default class Base {
    constructor() {
        this.main = new Main();
        this.skills = new Skills();
        this.projects = new Projects();
        //this.approach = new Approach();
        this.team = new Team();
        this.contacts = new Contacts();
        this.timeout = null;
        this.active = document.querySelector('.main');
        this.logo = document.querySelector('.logo');
        this.sections = document.querySelectorAll('.section');

        window.addEventListener('resize', this.windowResize);
        this.windowResize();

        document.addEventListener('mousewheel', this.scrolling.bind(this));
        document.addEventListener('DOMMouseScroll', this.scrolling.bind(this));
        document.addEventListener('touchstart', this.touchStart.bind(this));
        document.addEventListener('touchmove', this.scrolling.bind(this));
    }

    windowResize() {
        if (window.innerWidth < window.innerHeight) {
            window.mobile = true;
        }
        if (window.innerWidth > window.innerHeight) {
            window.mobile = false;
        }
    }

    touchStart(e) {
        this.touch_y = e.touches[0].pageY;
    }

    scrolling(e) {
        if (this.scroll_flag || window.mobile) return;
        this.scroll_flag = true;
        setTimeout(() => {
            this.scroll_flag = false;
        }, 1500);

        let delta = e.detail || -e.wheelDelta;
        if (e.deltaY == '-0') delta = 0;
        if (this.touch_y && e.touches) delta = this.touch_y - e.touches[0].pageY;

        if (delta > 0) this.nextPage();
        if (delta < 0) this.prevPage();
    }

    nextPage() {
        if (this.projects.active) {
            this.projects.nextProject();
            return;
        }
        if (this.skills.active) {
            this.skills.closeSkill();
            return;
        }
        const next = this.active.nextElementSibling;
        if (!next || this.nav_opened == true) return;
        location.hash = next.id;
    }

    prevPage() {
        if (this.projects.active) {
            this.projects.prevProject();
            return;
        }
        if (this.skills.active) {
            this.skills.closeSkill();
            return;
        }
        const prev = this.active.previousElementSibling;
        if (!prev || this.nav_opened == true) return;
        location.hash = prev.id;
    }

    goToPage(section) {
        this.active = document.querySelector(section);
        this.active.classList.remove('hidden');
        setTimeout(() => {
            this.active.classList.remove('up');
            this.active.classList.remove('down');
        }, 100);


        let sibling = this.active;
        while (sibling.previousSibling) {
            sibling = sibling.previousSibling;
            if (sibling.classList) {
                sibling.classList.remove('down');
                sibling.classList.add('up');
            }
        }
        sibling = this.active;
        while (sibling.nextSibling) {
            sibling = sibling.nextSibling;
            if (sibling.classList) {
                sibling.classList.remove('up');
                sibling.classList.add('down');
            }
        }

        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            for (let i = 0; i < this.sections.length; i++) {
                if (this.active !== this.sections[i]) this.sections[i].classList.add('hidden');
            }
        }, 1000);

        this.start();
    }

    start() {
        let name = this.active.id;
        name == 'main' ? this.logo.classList.add('hidden') : this.logo.classList.remove('hidden');
        document.body.className = this.active.getAttribute('data-tint');
        this[name].start();
    }
}