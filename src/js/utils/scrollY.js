export function scrollY(to, duration) {
    let flag = true;
    const
        element = document.scrollingElement || document.documentElement,
        start = element.scrollTop,
        change = to - start,
        startDate = +new Date(),
        easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        },
        animateScroll = function () {
            const currentDate = +new Date();
            const currentTime = currentDate - startDate;
            element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration));
            if (currentTime < duration) {
                if (flag) requestAnimationFrame(animateScroll);
            } else {
                element.scrollTop = to;
            }
        },
        stopScroll = function (e) {
            flag = false;
            window.removeEventListener('wheel', stopScroll);
        };
    animateScroll();
    window.addEventListener('wheel', stopScroll);
}