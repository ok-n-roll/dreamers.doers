export default class Nav {
    constructor(router) {

        this.router = router;

        this.body = document.body;
        this.nav = document.querySelector('.nav');
        this.hamburger = document.querySelector('.hamburger');
        this.projects = document.querySelector('.projects');
        this.scroll_top = window.pageYOffset;

        const nav_links = document.querySelectorAll('.nav a');
        for (let i = 0; i < nav_links.length; i++) {
            nav_links[i].addEventListener('click', this.navHandler.bind(this));
        }
        this.active = document.querySelector('.nav__link_sub').parentElement;

        if (window.mobile) {
            this.hamburger.addEventListener('touchstart', this.showHideNav.bind(this));
        } else {
            this.hamburger.addEventListener('click', this.showHideNav.bind(this));
        }

        document.querySelector('.main__arrow').addEventListener('click', this.mainArrowHandler.bind(this));
        document.querySelector('.logo').addEventListener('click', this.logoHandler.bind(this));
        window.addEventListener('hashchange', this.onHashChange.bind(this));
        this.onHashChange();
    }

    onHashChange() {
        const hash = location.hash.split('/');
        let section = hash[0];
        if (!section) {
            section = '#main';
        }
        if (window.mobile) {
            let section_offset = document.querySelector(section).offsetTop;
            if (section === '#contacts') section_offset += 50;
            window.scrollTo(0, section_offset);
        } else {
            this.router.goToPage(section);
        }

        if (this.router.projects.active) {
            this.router.projects.closeProject();
        }
        if (section === '#projects') {
            if (hash[1]) {
                this.router.projects.openProject(hash[1]);
            }
        }
    }

    navHandler(e) {
        e.preventDefault();

        const el = e.currentTarget;
        let href = el.getAttribute('href');

        if (el.classList.contains('nav__link_sub')) {
            const el_parent = el.parentElement;
            this.active.classList.add('overflow');
            if (this.active === el_parent) {
                el_parent.classList.toggle('active');
            } else {
                this.active.classList.remove('active');
                el_parent.classList.add('active');
            }
            this.active = el_parent;
            if (this.active.classList.contains('active')) {
                setTimeout(() => {
                    this.active.classList.remove('overflow');
                }, 300);
            }
            return false;
        }
        this.hideNav();
        if (el.classList.contains('projects-gallery__item')) {
            location.hash = 'projects/' + href;
        } else if (el.classList.contains('skills-item')) {
            location.hash = 'skills';
        } else {
            location.hash = el.getAttribute('href');
        }
    }

    showHideNav() {
        if (this.router.skills.active && window.mobile) {
            this.router.skills.closeSkill();
            return;
        }
        if (this.router.projects.active) {
            location.hash = 'projects';
            return;
        }
        if (this.router.nav_opened) {
            this.hideNav();
            if (window.mobile) {
                window.scrollTo(0, this.scroll_top);
            }
        } else {
            this.nav.classList.remove('hidden');
            this.hamburger.classList.add('close');

            if (window.mobile) {
                this.hamburger.classList.add('inactive');
                this.scroll_top = window.pageYOffset;
                setTimeout(() => {
                    this.body.classList.add('noscroll');
                    this.hamburger.classList.remove('inactive');
                }, 400);
            }

            this.router.nav_opened = true;
        }
    }

    hideNav() {
        this.router.nav_opened = false;
        this.nav.classList.add('hidden');
        this.hamburger.classList.remove('close');
        this.body.classList.remove('noscroll');

        document.documentElement.classList.add('no_transition');
        setTimeout(() => {
            document.documentElement.classList.remove('no_transition');
        }, 200);
    }

    logoHandler(e) {
        e.preventDefault();
        location.hash = 'main';
    }

    mainArrowHandler(e) {
        e.preventDefault();
        location.hash = 'projects';
    }
}