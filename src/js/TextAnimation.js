export default class TextAnimation {
    constructor(block) {
        if (!block.innerHTML) return;

        this.characters = ['Ặ', '🎓', 'ř', '&', 'Ĝ', 'æ', '¤', 'W', '¿', '◊', 'ị', '|', 'Q', '(', '*', '☁︎', '®', '🏓', 'Ů', 'u', 'Њ', 'Ế', 'Ҍ', '3', 'Ŗ', 'ĩ', '½', 'h', 'ĉ', 'Ō', 'ȯ', '7', 'ǯ', '₦', '¬', '‡', '●', 'Ǻ', 'Ể', 'm', '*', '1', 'ž', '§', 'ɐ', 'ḱ', 'ṁ', 'Ɣ', '%', 'Ǩ', 'Ó', '🍇', '*', 'd', '-', '🐨', 'ƣ', '5', 'ȧ', 'Œ', 'а', '<', 'Ḥ', 'ã', '⚾', '₮', '🍺', '♻', '🏉', 'Ť', '👟', '🎈', '$', 'ằ', '🖌', '2', '🛎', '🗄', '/', '~', '0', 'Ǣ', '8', '±', '₩', 'ϋ', 'ש', 'Ʃ', '🍒', 'Õ', 'ɔ', 'Ʒ', 'ï', 'ñ', 'ƻ', 'ⱴ', 'ḉ', 'ƙ', '🍀', 'ê', '&', 'ҩ', 'Ω', '🐰', '🦀', 'Ṻ', 'ҵ', 'Ω', 'Ø', 'Ș', '∆', '#', '🎸'];

        this.original = block.innerHTML.trim();
        this.block = block;
        this.len = this.block.textContent.trim().length;
        this.counter = 0;
        block.style.whiteSpace = 'nowrap';

        this.setRandomCharacters();
    }

    animationCharacters() {
        if (!this.original) return;
        this.counter++;
        this.setRandomCharacters();
        if (this.counter > 10) {
            setTimeout(e => this.setOriginalText(), 200);
        } else {
            setTimeout(e => requestAnimationFrame(this.animationCharacters.bind(this)), 150);
        }
    }

    setRandomCharacters() {
        this.res = '';
        let len = this.len - 1 || 1;
        for (let i = 0; i < len; i++) {
            this.res += this.characters[Math.floor(Math.random() * (this.characters.length - 2))];
        }
        this.block.innerHTML = this.res;
    }

    setOriginalText() {
        let c = 0,
            t = this.res;

        function setText() {
            t = t.substr(0, c) + this.original[c] + t.substr(c + 1, this.len);
            c++;
            this.block.innerHTML = t;
            if (c == this.len) {
                this.block.innerHTML = this.original;
                this.block.classList.remove('text-animation', 'project-text-animation');
                this.block.removeAttribute('style');
            } else {
                requestAnimationFrame(setText.bind(this));
            }
        }
        requestAnimationFrame(setText.bind(this));
    }
}