export default class Mobile {
    constructor(router) {

        this.router = router;
        this.temp = null;
        this.timeout;

        this.sections = document.querySelectorAll('.section');
        this.logo = document.querySelector('.logo');
        this.body = document.body;
        
        document.addEventListener('scroll', this.touchScroll.bind(this));
        document.addEventListener('touchend', this.touchScroll.bind(this));
        this.touchScroll();
    }

    touchScroll() {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            if (this.router.nav_opened || this.router.projects.active || !window.mobile || this.body.classList.contains('noscroll')) return;
            let _scrolled = window.pageYOffset;
            for (let i = 0; i < this.sections.length; i++) {
                if (this.sections[i].offsetTop + this.sections[i].offsetHeight >= _scrolled) {
                    if (this.temp != this.sections[i]) {
                        let name = this.sections[i].id;
                        this.router[name].start();
                        this.body.className = this.sections[i].getAttribute('data-tint');
                        if (!this.router.projects.active && !this.router.skills.active) {
                            name == 'main' ? this.logo.classList.add('hidden') : this.logo.classList.remove('hidden');
                            history.pushState(null, null, '#' + name);
                        }
                    }
                    this.temp = this.sections[i];
                    break;
                }
            }
        }, 200);
    }
}