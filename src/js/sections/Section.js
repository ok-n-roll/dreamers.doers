import TextAnimation from '../TextAnimation.js';

export default class Section {
    constructor(page) {
        this.section = document.querySelector('.' + page);
        this.finished = false;
        this.texts = this.section.querySelectorAll('.text-animation');
        this.text_animations = [];
        for (let i = 0; i < this.texts.length; i++) {
            let text = new TextAnimation(this.texts[i]);
            this.text_animations.push(text);
        }
    }

    start() {
        if (this.finished) return;

        setTimeout(() => {
            for (let i = 0; i < this.text_animations.length; i++) {
                this.text_animations[i].animationCharacters();
            }
        }, 1000);

        this.section.classList.remove('start');

        setTimeout(() => {
            this.finished = true;
        });
    }
}