import Section from './Section.js';
import TextAnimation from '../TextAnimation.js';
import ProjectsGallery from './ProjectsGallery.js';

export default class Projects extends Section {
    constructor() {
        super('projects');

        this.navProjects();
        new ProjectsGallery('.projects-gallery');
        
        this.body = document.body;
        this.list = document.querySelector('.projects__list');
        this.projects_gallery = document.querySelector('.projects-gallery');
        this.hamburger = document.querySelector('.hamburger');
        this.direction = null;

        let items = document.querySelectorAll('.projects-gallery__item');
        for (let i = 0; i < items.length; i++) {
            items[i].addEventListener('click', this.itemsHandler.bind(this));
        }
        this.active_nav_zoom = items[0];

        let right = this.list.querySelectorAll('.area.right'),
            left = this.list.querySelectorAll('.area.left'),
            down = this.list.querySelectorAll('.area.down'),
            play = this.list.querySelectorAll('.area.play');

        for (let i = 0; i < right.length; i++) {
            right[i].addEventListener('click', this.nextPage.bind(this));
            right[i].addEventListener('touchstart', this.touchStart.bind(this));
            right[i].addEventListener('touchend', this.touchEnd.bind(this));
            right[i].addEventListener('touchmove', this.touchMove.bind(this));
        }

        for (let i = 0; i < left.length; i++) {
            left[i].addEventListener('click', this.prevPage.bind(this));
            left[i].addEventListener('touchstart', this.touchStart.bind(this));
            left[i].addEventListener('touchend', this.touchEnd.bind(this));
            left[i].addEventListener('touchmove', this.touchMove.bind(this));
        }

        for (let i = 0; i < down.length; i++) {
            down[i].addEventListener('click', this.nextProject.bind(this));
            down[i].addEventListener('touchstart', this.nextProject.bind(this));
        }

        for (let i = 0; i < play.length; i++) {
            play[i].addEventListener('touchstart', this.touchStart.bind(this));
            play[i].addEventListener('touchend', this.touchEnd.bind(this));
            play[i].addEventListener('touchmove', this.touchMove.bind(this));
        }
    }

    start() {
        super.start();
        if (this.finished) return;
        setTimeout(() => {
            document.querySelector('.projects__gallery').classList.remove('start');
        }, 2000);
    }

    itemsHandler(e) {
        e.preventDefault();
        const target = e.currentTarget;
        this.active_nav_zoom = target;
        this.active_nav_zoom.classList.add('zoom');
        location.hash = 'projects/' + target.getAttribute('href');
    }

    openProject(name) {
        this.active_nav = this.projects_gallery.querySelector(`[href="${name}"]`);
        this.active = document.querySelector('.' + name);
        if (this.direction) this.active.classList.add(this.direction);
        this.page_index = 0;
        this.len = this.active.children.length;
        this.list.classList.remove('hidden');
        this.active.classList.remove('hidden');

        setTimeout(() => {
            this.hamburger.classList.add('close');
            this.active.classList.add('active');
            this.active.classList.remove('up');
            this.active.classList.remove('down');
            this.body.classList.add('noscroll');
            this.direction = null;
        }, 300);

        if (window.mobile) {
            let bg_images = this.active.querySelectorAll('div[data-img-m]');
            for (let i = 0; i < bg_images.length; i++) {
                bg_images[i].style.backgroundImage = 'url(' + bg_images[i].getAttribute('data-img-m') + ')';
            }
        } else {
            let bg_images = this.active.querySelectorAll('div[data-img]');
            for (let i = 0; i < bg_images.length; i++) {
                bg_images[i].style.backgroundImage = 'url(' + bg_images[i].getAttribute('data-img') + ')';
            }
        }

        this.videos = this.active.querySelectorAll('video');
        for (let i = 0; i < this.videos.length; i++) {

            let video = this.videos[i];
            if (!video.getAttribute('autoplay') && !window.mobile || video.classList.contains('content-video')) {
                let src = video.textContent;
                let source = document.createElement('source');
                video.setAttribute('preload', '');
                video.setAttribute('autoplay', 'autoplay');
                video.setAttribute('loop', '');
                video.setAttribute('muted', '');
                source.setAttribute('src', src);
                video.innerHTML = '';
                video.appendChild(source);
                video.pause();
            }
        }

        this.project_texts = this.active.querySelectorAll('.project-text-animation');
        if (this.project_texts.length) {
            this.project_text_animations = [];
            for (let i = 0; i < this.project_texts.length; i++) {
                let text = new TextAnimation(this.project_texts[i]);
                this.project_text_animations.push(text);
            }
            setTimeout(() => {
                for (let i = 0; i < this.project_text_animations.length; i++) {
                    this.project_text_animations[i].animationCharacters();
                }
            }, 1600);
        }

        let all_pages = this.active.querySelectorAll('.page');
        this.pages = [];
        for (let i = 0; i < all_pages.length; i++) {
            this.pages.push(all_pages[i]);
        }

        this.sides = this.active.querySelectorAll('.side');
        this.changePage();
    }

    closeProject() {
        if (!this.active) return;
        this.list.classList.add('hidden');
        this.active.classList.remove('active');
        this.active_nav_zoom.classList.remove('zoom');
        this.hamburger.classList.remove('close');
        if (this.active_side) this.active_side.classList.add('hidden');
        this.body.classList.remove('noscroll');

        if (this.project_texts.length) {
            for (let i = 0; i < this.project_text_animations.length; i++) {
                this.project_text_animations[i].animationCharacters();
            }
        }

        let active_project = this.active;
        setTimeout(() => {
            active_project.removeAttribute('style');
            active_project.classList.add('hidden');
        }, 1000);


        if (this.active_video) {
            this.active_video.pause();
        }

        this.active = null;
    }

    nextProject() {
        let next = this.active_nav.nextElementSibling;
        if (next) {
            this.direction = 'down';
            location.hash = 'projects/' + next.getAttribute('href');
        } else {
            location.hash = 'projects';
        }
    }

    prevProject() {
        let prev = this.active_nav.previousElementSibling;
        if (prev) {
            this.direction = 'up';
            location.hash = 'projects/' + prev.getAttribute('href');
        } else {
            location.hash = 'projects';
        }
    }

    nextPage() {
        this.page_index++;
        if (this.page_index >= this.len) {
            this.page_index = this.len;
            return;
        }
        this.changePage();
    }

    prevPage() {
        this.page_index--;
        if (this.page_index < 0) {
            this.page_index = 0;
            return;
        }
        this.changePage();
    }

    changePage() {
        const active_page = this.pages[this.page_index];
        if (this.active) this.active.classList.add('cur-fix');
        if (this.active_video)
            this.active_video.pause();
        this.active_video = active_page.querySelector('video');

        setTimeout(() => {
            for (let i = 0; i < this.sides.length; i++) {
                this.sides[i].classList.add('hidden');
            }
            this.active_side = active_page.querySelector('.side');
            if (this.active_side)
                this.active_side.classList.remove('hidden');
        }, 400);

        setTimeout(() => {
            if (this.active) {
                this.active.classList.remove('cur-fix');
            }
            if (this.active_video) {
                this.active_video.play();
            }
        }, 1010);

        if (this.active) this.active.style.transform = 'translateX(-' + this.page_index * 100 + 'vw)';
        document.body.className = active_page.getAttribute('data-tint');
    }

    navProjects() {
        let projects = document.querySelector('.projects__gallery').cloneNode(true);
        projects.classList.remove('projects__gallery', 'start');
        document.querySelector('.nav__projects').appendChild(projects);
    }

    touchStart(e) {
        this.touch_x = e.touches[0].pageX;
        this.touch_y = e.touches[0].pageY;
        this.touch_flag = true;
    }

    touchEnd(e) {
        this.touch_flag = false;
    }

    touchMove(e) {
        e.preventDefault();
        if (!this.touch_flag) return;
        let dif = this.touch_x - e.touches[0].pageX,
            dif_y = this.touch_y - e.touches[0].pageY;

        if (dif > 20) {
            this.nextPage();
            this.touch_flag = false;
        }
        if (dif < -20) {
            this.prevPage();
            this.touch_flag = false;
        }


        if (dif_y > 50) {
            this.nextProject();
            this.touch_flag = false;
        }
        if (dif_y < -50) {
            this.prevProject();
            this.touch_flag = false;
        }
    }
}