import Section from './Section.js';

export default class Main extends Section {
    constructor() {
        super('main');
        this.hamburger = document.querySelector('.hamburger');
        setTimeout(e => {
            this.hamburger.classList.remove('start')
        }, 4000);
    }

    start() {
        super.start();
        if (this.finished) return;

        let text = document.querySelectorAll('.main__d-text'),
            svg = document.querySelectorAll('.main__d-svg');

        setTimeout(() => {
            for (let i = 0; i < text.length; i++) {
                text[i].parentElement.removeChild(text[i]);
                svg[i].style.display = 'block';
            }
        }, 2800);
    }
}