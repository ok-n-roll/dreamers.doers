import Section from './Section.js';

export default class Team extends Section {
    constructor() {
        super('team');
    }

    start() {
        super.start();
        if (this.finished) return;

        let photos = document.querySelectorAll('.team-item__photo_distortion');

        setTimeout(() => {
            for (let i = 0; i < photos.length; i++) {
                photos[i].classList.add('hidden');
            }
        }, 2000);
    }
}