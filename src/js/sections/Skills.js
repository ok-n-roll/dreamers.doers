import Section from './Section.js';

export default class Skills extends Section {
    constructor() {
        super('skills');

        this.navSkills();

        this.body = document.body;
        this.hamburger = document.querySelector('.hamburger');
        this.logo = document.querySelector('.logo');
        this.close = document.querySelector('.skills__close');
        const items = document.querySelectorAll('.skills-item');
        this.active_item = items[0];
        this.active = null;
        for (let i = 0; i < items.length; i++) {
            if (!items[i].classList.contains('innactive')) {
                items[i].addEventListener('click', this.itemsHandler.bind(this));
            }
        }
        this.close.addEventListener('click', this.closeSkill.bind(this));

        this.closeSkill = this.closeSkill.bind(this);
    }

    start() {
        super.start();
    }

    navSkills() {
        let items = document.querySelector('.skills__items').cloneNode(true);
        items.classList.remove('skills__items');
        items.classList.add('nav__skills-items');
        document.querySelector('.nav__skills').appendChild(items);
    }

    itemsHandler(e) {
        e.preventDefault();
        if (this.active) {
            this.closeSkill();
        }
        this.active_item = e.currentTarget;
        this.active = document.querySelector(this.active_item.getAttribute('href'));
        this.openSkill();
    }

    openSkill() {
        this.hamburger.classList.add('invert');
        this.close.classList.remove('hidden');
        this.active.classList.remove('hidden');
        this.active_item.classList.add('zoom');
        this.logo.addEventListener('click', this.closeSkill.bind(this));

        if (window.mobile) {
            this.page_offset = window.pageYOffset || document.documentElement.scrollTop;
            setTimeout(() => {
                this.body.classList.add('noscroll');
                this.body.classList.add('dark');
                this.body.classList.remove('light');
                this.hamburger.classList.add('close');
            }, 300);
        } else {
            this.video = this.active.querySelector('video');
            if (this.video) {
                if (this.video.loop) {
                    this.video.play();
                } else {
                    this.makeVideo();
                }
            }
        }
        setTimeout(() => {
            location.hash = 'skills';
        });
    }

    closeSkill() {
        this.hamburger.classList.remove('invert');
        this.close.classList.add('hidden');
        this.logo.removeEventListener('click', this.closeSkill.bind(this));
        if (window.mobile) {
            this.body.classList.remove('noscroll');
            this.body.classList.remove('dark');
            this.body.classList.add('light');
            if (!this.page_offset) {
                this.page_offset = document.querySelector('.skills').offsetTop + 60;
            }
            window.scrollTo(0, this.page_offset);
        }
        if (this.active) {
            this.active.classList.add('hidden');
            this.active = null;
        }
        if (this.video) {
            this.video.pause();
        }
        this.active_item.classList.remove('zoom');
        this.hamburger.classList.remove('close');
    }

    makeVideo() {
        let src = this.video.textContent;
        let source = document.createElement('source');
        this.video.setAttribute('preload', '');
        this.video.setAttribute('autoplay', 'autoplay');
        this.video.setAttribute('loop', '');
        this.video.setAttribute('muted', '');
        source.setAttribute('src', src);
        this.video.innerHTML = '';
        this.video.appendChild(source);
    }
}