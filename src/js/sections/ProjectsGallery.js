export default class ProjectsGallery {
    constructor() {
        this.gallery = document.querySelectorAll('.projects-gallery__items');
        this.brands = document.querySelector('.projects-brands__items');
        this.skills = document.querySelector('.nav__skills-items');
        document.addEventListener('mousemove', this.mouseMove.bind(this), false);

        let items = document.querySelectorAll('.projects-gallery__item');
        for (let i = 0; i < items.length; i++) {
            items[i].addEventListener('mousemove', this.titleCursor);
        }
    }

    mouseMove(e) {
        let ww = window.innerWidth;
        if (window.mobile) return; 
        let gw = this.gallery[1].offsetWidth;
        let bw = this.brands.offsetWidth;
        let sw = this.skills.scrollWidth;
        if (gw <= ww) return;
        let pos = gw > ww ? linear(e.pageX, 0, ww, 40, ww - gw - 80) : 0;
        let posBrands = bw > ww ? linear(e.pageX, 0, ww, 0, ww - bw) : 0;
        let posSkills = linear(e.pageX, 0, ww, 40, ww - sw - 80);
        this.gallery[0].style.transform = 'translateX('+ pos +'px)';
        this.gallery[1].style.transform = 'translateX(' + pos + 'px)';
        this.brands.style.transform = 'translateX(' + posBrands + 'px)';
        this.skills.style.transform = 'translateX(' + posSkills + 'px)';
    }

    titleCursor(e) {
        let el = e.target,
            pos = el.getBoundingClientRect(),
            title = el.querySelector('.projects-gallery__title');
        title.style.transform = 'translate(' + (e.pageX - pos.left) + 'px, ' + (e.pageY - pos.top) + 'px)';
    }
}

function linear(x, x1, x2, y1, y2) {
    var val = (x - x1)/(x2 - x1)*(y2-y1) + y1; 
    val = Math.min(val, Math.max(y1, y2)); 
    val = Math.max(val, Math.min(y1, y2)); 
    return val; 
}