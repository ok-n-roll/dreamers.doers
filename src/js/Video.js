export default class Video {
    constructor(block) {
        this.container = document.querySelector('#' + block);
        this.page = this.container.parentElement.parentElement;
        this.video_container = this.container.parentElement;
              
        this.block = block;
        this.id = this.container.getAttribute('data-id');
        this.video = this.page.querySelector('.video');
        this.play = this.page.querySelector('.area.play');
        this.close = this.page.querySelector('.area.close');
        this.hamburger = document.querySelector('.hamburger');
        this.logo = document.querySelector('.logo');
        this.side = this.page.querySelector('.side') || this.page.querySelector('.skill__side');

        this.play.addEventListener('click', this.fullVideo.bind(this));
        this.close.addEventListener('click', this.stopVideo.bind(this));
    }

    build() {
        this.player = new YT.Player(this.block, {
            videoId: this.id,
            host: 'https://www.youtube.com',
            width: '100%',
            height: '100%',
            playerVars: {
                autoplay: 0,
                mute: 0,
                color: 'white',
                playlist: this.id,
                controls: 0,
                disablekb: 1,
                loop: 1,
                rel: 0,
                showinfo: 0,
                modestbranding: 1
            },
            events: {
                onReady: this.init.bind(this),
                onStateChange: this.onStateChange.bind(this)
            }
        });

        let close = document.createElement('div');
        close.className = 'hamburger close video-close';
        this.video_container.appendChild(close);
        close.addEventListener('click', this.stopVideo.bind(this));
    }

    init() {
        this.loaded = true;
        this.startVideo();
    }

    fullVideo() {
        this.page.classList.add('playback');
        this.side.classList.add('hidden');
        this.hamburger.classList.add('hidden');
        this.logo.classList.add('white');
        this.close.classList.remove('hidden');

        if (this.loaded) {
            this.startVideo();
        } else {
            this.build();
        }

        document.addEventListener('mousewheel', this.scrolling.bind(this));
        document.addEventListener('DOMMouseScroll', this.scrolling.bind(this));
    }

    startVideo() {
        if (!this.loaded) return;
        this.player.playVideo();
    }

    stopVideo() {
        if (!this.loaded) return;
        this.page.classList.remove('playback');
        this.side.classList.remove('hidden');
        this.hamburger.classList.remove('hidden');
        this.close.classList.add('hidden');
        this.logo.classList.remove('white');
        this.player.pauseVideo();
    }

    scrolling(e) {
        document.removeEventListener('mousewheel', this.scrolling.bind(this));
        document.removeEventListener('DOMMouseScroll', this.scrolling.bind(this));
        this.stopVideo();
    }

    onStateChange(e) {
        if (e.data === 2) {
            this.stopVideo();
        }
    }
}