import Base from './Base.js';
import Nav from './Nav.js';
import Mobile from './Mobile.js';
import Cursor from './Cursor.js';
import Video from './Video.js';

window.onload = () => {
    const router = new Base();
    new Nav(router);
    new Mobile(router);
    new Cursor();

    const videos = document.querySelectorAll('.video-id');
    for (let i = 0; i < videos.length; i++) {
        new Video(videos[i].id);
    }
}