export default class Cursor {
    constructor() {
        this.options = ['right', 'left', 'down', 'play', 'close', 'invert'];
        
        this.cursor = document.querySelector('.cursor');
        this.areas = document.querySelectorAll('.area');
        //this.hidden = true;
        for (let i = 0; i < this.areas.length; i++) {
            this.areas[i].addEventListener('mouseover', this.onAreaOver.bind(this));
            this.areas[i].addEventListener('mouseout', this.onAreaOut.bind(this));
        }
        document.addEventListener('mousemove', this.cursorPosition.bind(this));
    }

    cursorPosition(e) {
        if (window.mobile) return;
        let x = e.pageX;
        let y = e.pageY;
        this.cursor.style.transform = `translate(${x}px, ${y}px)`;
    }

    onAreaOver(e) {
        //this.hidden = false;
        const area = e.currentTarget;
        for (let i = 0; i < this.options.length; i++) {
            this.cursor.classList.remove(this.options[i]);
            if (area.classList.contains(this.options[i])) {
                this.cursor.classList.add(this.options[i]);
            }
        }
        this.cursor.classList.remove('hidden');
    }

    onAreaOut(e) {
        //this.hidden = true;
        this.cursor.classList.add('hidden');
    }
}