var webRoot = 'www/';
var srcRoot = 'src/';

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    svgmin = require('gulp-svgmin'),
    livereload = require('gulp-livereload'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    modules = require('gulp-es6-module-transpiler'),
    concat = require('gulp-concat');

gulp.task('css', function () {
    gulp.src(srcRoot + 'scss/application.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cssmin())
        .pipe(rename('app.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(webRoot + 'assets/css/'))
        .pipe(livereload());
});

gulp.task('js', function () {
    gulp.src(srcRoot + 'js/app.js')
        .pipe(plumber())
        .pipe(modules({
            formatter: 'bundle'
        }))
        .pipe(babel({
            minified: true,
            comments: false,
            presets: ['env']
        }))
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest(webRoot + 'assets/js/'));
});

gulp.task('svg', function () {
    gulp.src(srcRoot + 'svg/**/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest(webRoot + 'assets/svg/'));
});

gulp.task('html', function () {
    gulp.src([
        srcRoot + 'html/header.html',
        srcRoot + 'html/sections/main.html',
        srcRoot + 'html/sections/projects.html',
            srcRoot + 'html/projects/uaz-profi.html',
            srcRoot + 'html/projects/two-legends.html',
            srcRoot + 'html/projects/influence-marketing.html',
            srcRoot + 'html/projects/wargaming.html',
            srcRoot + 'html/projects/brand-platform.html',
            srcRoot + 'html/projects/uaz-global.html',
            srcRoot + 'html/projects/strategy.html',
            srcRoot + 'html/projects/btl.html',
        srcRoot + 'html/sections/projects-end.html',
        srcRoot + 'html/sections/skills.html',
            srcRoot + 'html/skills/1.html',
            srcRoot + 'html/skills/2.html',
            srcRoot + 'html/skills/3.html',
            srcRoot + 'html/skills/4.html',
            srcRoot + 'html/skills/5.html',
            srcRoot + 'html/skills/6.html',
            srcRoot + 'html/skills/7.html',
            srcRoot + 'html/skills/8.html',
            srcRoot + 'html/skills/9.html',
            srcRoot + 'html/skills/10.html',
            srcRoot + 'html/skills/11.html',
            srcRoot + 'html/skills/12.html',
            srcRoot + 'html/skills/13.html',
            srcRoot + 'html/skills/14.html',
        srcRoot + 'html/sections/skills-end.html',
        srcRoot + 'html/sections/team.html',
        srcRoot + 'html/sections/contacts.html',
        srcRoot + 'html/nav.html',
        srcRoot + 'html/footer.html'
        ])
        .pipe(concat('index.html'))
        .pipe(gulp.dest(webRoot));
});

gulp.task('watch', ['css', 'svg', 'js', 'html'], function () {
    gulp.watch(srcRoot + 'scss/**/*.scss', ['css']);
    gulp.watch(srcRoot + 'svg/*.svg', ['svg']);
    gulp.watch(srcRoot + 'js/**/*.js', ['js']);
    gulp.watch(srcRoot + 'html/**/*.html', ['html']);
    livereload.listen();
});